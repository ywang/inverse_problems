;
; purpose: to solve example 3.4
; author: Yi Wang
; date: 02/02/2014
;

d = [5.973, 8.02]

sigma = [0.0005, 0.005]

Re = 6370.8 ; km

N = 101
r_sample = FindGen(N) / (N-1)
q1 = int_tabulated(r_sample, g1(r_sample)) ; (3.49)
q2 = int_tabulated(r_sample, g2(r_sample)) ; (3.49)
q = [q1, q2]

;;;;;;;;;;;;;
; r = 1000km
;;;;;;;;;;;;;
;(3.53)
r1 = 1000.0 / Re
H1 = FltArr(2, 2)
H1[0,0] = int_tabulated(r_sample, g1(r_sample) * g1(r_sample) * (r_sample-r1)^2)
H1[1,0] = int_tabulated(r_sample, g1(r_sample) * g2(r_sample) * (r_sample-r1)^2)
H1[0,1] = int_tabulated(r_sample, g2(r_sample) * g1(r_sample) * (r_sample-r1)^2)
H1[1,1] = int_tabulated(r_sample, g2(r_sample) * g2(r_sample) * (r_sample-r1)^2)

; (3.53)
lamda1 = 1.0 / (q ## invert(H1) ## transpose(q))
c1 = invert(H1) ## transpose(q) * [[lamda1], [lamda1]]

print, 'Coefficients for ri = ', r1, '(', r1*Re, ' km) are: '
print, c1
print, 'Estimate for ri = ', r1, ' is: '
print, d ## c1 ; (3.40)
print, 'Standard deviatoin for ri = ', r1, ' is: '
print, sqrt( total( reform(transpose(c1))^2 * sigma^2 ) );

; (3.44)
A1 = c1[0,0] * g1(r_sample) + c1[0,1] * g2(r_sample)



;;;;;;;;;;;;;
; r = 5000km
;;;;;;;;;;;;;
;(3.53)
r5 = 5000.0 / Re
H5 = FltArr(2, 2)
H5[0,0] = int_tabulated(r_sample, g1(r_sample) * g1(r_sample) * (r_sample-r5)^2)
H5[1,0] = int_tabulated(r_sample, g1(r_sample) * g2(r_sample) * (r_sample-r5)^2)
H5[0,1] = int_tabulated(r_sample, g2(r_sample) * g1(r_sample) * (r_sample-r5)^2)
H5[1,1] = int_tabulated(r_sample, g2(r_sample) * g2(r_sample) * (r_sample-r5)^2)

; (3.53)
lamda5 = 1.0 / (q ## invert(H5) ## transpose(q))
c5 = invert(H5) ## transpose(q) * [[lamda5], [lamda5]]

print, 'Coefficients for ri = ', r5, '(', r5*Re, ' km) are: '
print, c5
print, 'Estimate for ri = ', r5, ' is: '
print, d ## c5 ; (3.40)
print, 'Standard deviatoin for ri = ', r5, ' is: '
print, sqrt( total( reform(transpose(c5))^2 * sigma^2 ) );

; (3.44)
A5 = c5[0,0] * g1(r_sample) + c5[0,1] * g2(r_sample)


; figure 3.5
plot, r_sample*Re, A1,	$
	/nodata,	$
	charsize = 2.0,	$
	xstyle = 1,	$
	ystyle = 1,	$
	xrange = [0, 7000], $
	yrange = [-1, 2.5], $
	Xtitle = 'radius(km)', $
	Ytitle = 'A(r)'
oplot, r_sample*Re, A1, color=cgcolor('red')
oplot, r_sample*Re, A5, color=cgcolor('blue')

legend, halign = 0.1, valign = 0.9,	$
	charsize = 2.0, $
	line = [0, 0],	$
	lcolor = [cgcolor('red'), cgcolor('blue')], $
	label =['r=1000km', 'r=5000km']


screen2PNG, 'fig3.5.png'


end
