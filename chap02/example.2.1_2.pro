;
; purpose: to solve problem 2.1, 2.2
; author: Yi Wang
; date: Dec. 22, 2014
;



; 
; example 2.1
;

obs_err = 8.0

G = [ [1.0,  1.0,  -0.5], $
      [1.0,  2.0,  -2.0], $
      [1.0,  3.0,  -4.5], $
      [1.0,  4.0,  -8.0], $
      [1.0,  5.0, -12.5], $
      [1.0,  6.0, -18.0], $
      [1.0,  7.0, -24.5], $
      [1.0,  8.0, -32.0], $
      [1.0,  9.0, -40.5], $
      [1.0, 10.0, -50.0] ]      ; (2.35)
print, 'G matrix: '
print, G

d = [109.4, 187.5, 267.5, 331.9, 386.1, 428.4, 452.2, 498.1, 512.3, 513.0] ; (table 2.1)
d = transpose( d )
print, 'observation: '
print, d

Gw = G / obs_err
dw = d / obs_err
m_L2 = invert( transpose( Gw ) ## Gw ) ## transpose( Gw ) ## dw ; (2.18, 2.38)
print, 'least square solution of model:'
print, m_L2

cov_m_L2 = invert( transpose( Gw ) ## Gw ) ; (2.24, 2.37)
print, 'model covariance matrix: '
print, cov_m_L2

chi_square = total( (d - (G ## m_L2)) ^ 2 / obs_err ^ 2) ; (2.20)
print, 'chi_square value: ', chi_square

print, 'p value: ', 1.0 - chisqr_pdf( chi_square, 10-3 ) ; (2.39)

; figure 2.1
x = FindGen(100) / (100-1) * 10
y = m_L2[0] + m_L2[1]*x - 0.5*m_L2[2]*x^2
window, 1
multipanel, rows=1, cols=1
plot, x, y,	$
	xtitle = 'Time (s)', $
	ytitle = 'Elevation (m)'
oplot, G[1,*], d, psym = 5
screen2PNG, 'fig2.1.png'
wdelete, 1


;
; example 2.2
;

; inv_cov_m_L2 must be symmetric. 

inv_cov_m_L2      = invert(cov_m_L2)
inv_cov_m_L2[1,0] = inv_cov_m_L2[0,1]
inv_cov_m_L2[2,0] = inv_cov_m_L2[0,2]
inv_cov_m_L2[2,1] = inv_cov_m_L2[1,2]
evalues = eigenql( inv_cov_m_L2, eigenvectors = evecs) ; (2.42, 2.47, 2.48)

print, 'direction of semiaxes for error ellipsoid: '
print, transpose( evecs )

print, 'corresponding eigenvalues: '
print, transpose( evalues )

length = chisqr_cvf( 1-0.95, 3 )
length = sqrt( length )
length = length / sqrt( evalues ) ; (2.49)
print, 'the corresponding 95% confidence ellipsoid semiaxis length: '
print,  transpose( length )

rebin_length = rebin( transpose( length ), 3, 3 )

proj = evecs * rebin_length

print, 'projecting the 95% confidence ellipsoid into the coordinate system: ' ; (2.50)
print, max( abs( proj[0,*] ) )
print, max( abs( proj[1,*] ) )
print, max( abs( proj[2,*] ) )


;
; Monte Carlo Section
;
N = 10000
d0 = G ## transpose(m_L2)

chimic = FltArr(N)

mtrail = FltArr(N, 3)

pmic = FltArr(N)

for i = 0, N-1, 1 do begin
     t = i
     dtrail = d0 + obs_err * transpose(randomn(t, 10))
     dwtrail = dtrail / obs_err
     mtrail[i, *] = invert( transpose( Gw ) ## Gw ) ## transpose( Gw ) ## dwtrail
;     chimic[i] = total(((dtrail - d0) / obs_err) ^ 2)
     chimic[i] = total(((dtrail - G ## mtrail[i, *]) / obs_err) ^ 2)
     pmic[i] = 1.0 - chisqr_pdf( chimic[i], 10-3 )
endfor

meanmtrail = total(mtrail, 1) / N
diffmtrail = mtrail - rebin(transpose(meanmtrail), N, 3)

mc_cov = diffmtrail ## transpose(diffmtrail) / N
print, 'empirical estimate of the covariance matrix:'
print, mc_cov


window, 2
multipanel, rows=1, cols=1
CgHistoplot, chimic, xtitle = 'chi-square statistic', charsize = 2
screen2PNG, 'fig2.p1.png'
wdelete, 2

window, 2
multipanel, rows=1, cols=1
CgHistoplot, pmic, xtitle = 'p-value', charsize = 2
screen2PNG, 'exercise.2.4.png'
wdelete, 2

window, 3
multipanel, rows=1, cols=3
CgHistoplot, mtrail[*,0], Charsize = 3, title = 'm1'
CgHistoplot, mtrail[*,1], Charsize = 3, title = 'm2'
CgHistoplot, mtrail[*,2], Charsize = 3, title = 'm3'
screen2PNG, 'fig2.p2.png'
wdelete, 3

window, 4
multipanel, rows=1, cols=3

plot, mtrail[*,0], mtrail[*,1], $
	charsize = 3,	$
	xtitle = 'm1',	$
	ytitle = 'm2',	$
	xrange = [-20, 60], $
	yrange = [80, 120], $
	psym = 2

plot, mtrail[*,0], mtrail[*,2], $
        charsize = 3,   $
        xtitle = 'm1',  $
        ytitle = 'm3',  $
	xrange = [-20, 60], $
	yrange = [6, 12], $
        psym = 2

plot, mtrail[*,1], mtrail[*,2], $
        charsize = 3,   $
        xtitle = 'm2',  $
        ytitle = 'm3',  $
	xrange = [80, 120], $
	yrange = [6, 12], $
        psym = 2


screen2PNG, 'fig2.p3.png'
wdelete, 4


stop





end
