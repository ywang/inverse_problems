;
; purpose: to solve example 2.3
; author: Yi Wang
; date: Jan 23, 2015
;

N = 125

x = 25.0 + FindGen(N)*3.0
x = transpose(x)

y_true = 10.0 * x + 0.0
y_obs  = y_true + 0.05 * randomn(10, N) * y_true

; (2.55)
G = FltArr(2, N)
G[0,*] = 1
G[1,*] = x

m = invert( transpose(G) ## G ) ## transpose(G) ## y_obs ; (2.24, 2.57)
print, 'estimate of model:'
print, m

r = y_obs - G ## m ; (2.51)

s = sqrt( total(r^2) / (N-2)) ; (2.51)
print, 'estimate of observation stand deviation:'
print, s

C = s ^ 2  * invert( transpose(G) ## G ) ; (2.58)
print, 'estimated covariance matrix:'
print, C

; (2.59, 2.60)
inv = sqrt( diag_matrix(C) ) * t_cvf(1-0.975, N-2)
print, 'confidence interval:'
print, inv

; figure 2.3
window, 1
y_pret = m[0] + m[1] * x
plot, x, y_pret, $
   xtitle = 'x', $
   ytitle = 'y'
oplot, x, y_obs, psym = 5

; figure 2.4
window, 2
plot, x, r, $
   psym = 5, $
   xtitle = 'x', $
   ytitle = 'r'

; (2.61)
W = invert( diag_matrix(y_obs) )
Gw = W ## G
yw = W ## y_obs

; (2.62)
mw = invert( transpose(Gw) ## Gw ) ## transpose(Gw) ## yw
print, 'estimated model'
print, mw

; (2.63, 2.64)
y_pretw = Gw ## transpose(mw)
rw = y_pretw - yw
sw = sqrt( total(rw^2) / (N-2))
print, 'estimated standard deviation:'
print, sw
Cw = sw^2 * invert( transpose(Gw) ## Gw )
invw = sqrt( diag_matrix(Cw) ) * t_cvf(1-0.975, N-2)
print, 'confidence interval:'
print, invw

; figure 2.5
y_pretw = G ## transpose(mw)
window, 3
plot, x, y_pretw, $
   xtitle = 'x', $
   ytitle = 'y'
oplot, x, y_obs, psym = 5

; figure 2.6
window, 4
plot, x, rw, $
   psym = 5, $
   xtitle = 'x', $
   ytitle = 'r'


stop



end
