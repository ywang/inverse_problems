;
; purpose: to solve example 4.1
; author: Yi Wang
; date: Jul 19, 2015
;

; Notes:
; I don't know why the result is different from that in the book

t = sqrt(2.0)

; G in function 4.94
G = [ [1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0], $
      [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0], $
      [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0], $
      [1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], $
      [0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0], $
      [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0], $
      [  t, 0.0, 0.0, 0.0,   t, 0.0, 0.0, 0.0,   t], $
      [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,   t] ]

SVDC, G, W, U, V

;----------------------------
; sort W, U, V desecendingly 
;----------------------------
dim = size( G )
m   = dim[2]
n   = dim[1]
rank = sort(W)
rank = reverse( rank )
W = W[rank]
for i = 0, m-1, 1 do begin
   tmp_U = U[*,i]
   U[*,i] = tmp_U[rank]
endfor
for i = 0, n-1, 1 do begin
   tmp_V= V[*,i]
   V[*,i] = tmp_V[rank]
endfor

print, 'Singular values: '
print, W

;----------------------
; N(G), function 4.96
;----------------------
V8 = V[7,*]
V9 = V[8,*]
reshape_V8 = reform(V8, 3, 3)
reshape_V9 = reform(V9, 3, 3)


open_mydevice, filename = "example.4.1.ps", /portrait

myct, 63, /reverse, ncolors = 101

multipanel, rows=3, cols=2

x = [1.0, 2.0, 3.0]
y = x
Tvplot, reshape_V8, x, y,$
	/sample


;----------------
; function 4.100
;----------------
; model resolution matrix
Rm = V[0:6,*]##transpose(V[0:6,*])
reshape_diag_Rm = reform( diag_matrix( Rm ), 3, 3 )
print, '------------------------------'
print, 'Reshaped diagnoal of model resolution matrix'
print, reshape_diag_Rm

;---------------------
; function 4.101
;---------------------
m_test = [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0]
m_test = transpose( m_test )
d_test = G ## m_test
inv_S = FltArr(7, 7)
for i = 0, 6, 1 do inv_S[i,i] = 1.0 / W[i]
m_inverse = V[0:6,*] ## inv_S ## transpose( U[0:6,*] ) ## d_test
print, '-------------------------------'
print, 'm_test: '
print, m_test
print, 'd_test: '
print, d_test
print, 'Reshaped 5th col model resolution matrix: '
print, reform( Rm[4,*], 3, 3 )
print, 'The generalized inverse solution for the noise-free spike test: '
print, m_inverse







close_mydevice

stop

end
