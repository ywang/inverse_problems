;
; purpose: to slove example 4.2
; author: Yi Wang
; date: Jul 20, 2015
;
; Note:
; (1) I just follow the MATLAB code which is dfferent functions in the book

m = 210
n = m

delta_t = 0.5

T0 = 10.0

t = -5.0 + FindGen(m+1) * delta_t


response_g = FltArr(n)
for j = 0, n-1, 1 do begin

   if ( t[j] lt 0.0 ) then begin
      response_g[j] = 0.0
   endif else begin
      response_g[j] = t[j] * exp( -t[j] / T0 )
   endelse

endfor
response_g_max = max( response_g )
response_g = response_g / response_g_max

open_mydevice, filename = "example.4.2.ps", /portrait

multipanel, rows=3, cols=2

; figure 4.9
plot, t, response_g, $
        color = cgcolor('black'), $
        xtitle = 'Times (s)',   $
        ytitle = 'V',$
        xrange = [-5,100],$
        xstyle = 1



G = FltArr(n, m)
for i = 1, m, 1 do begin
for j = 0, n-1, 1 do begin

   tp = t[j] - t[i]

   if ( tp ge 0.0 ) then begin
      G[j,i-1] = 0.0
   endif else begin
      G[j,i-1] = -tp * exp( tp / T0 )
   endelse

endfor
endfor
G = G / response_g_max * delta_t
G = double(G)

SVDC, G, W, U, V

;----------------------------
; sort W, U, V desecendingly 
;----------------------------
rank = sort(W)
rank = reverse( rank )
W = W[rank]
for i = 0, m-1, 1 do begin
   tmp_U = U[*,i]
   U[*,i] = tmp_U[rank]
endfor
for i = 0, n-1, 1 do begin
   tmp_V= V[*,i]
   V[*,i] = tmp_V[rank]
endfor



; figure 4.10
x = FindGen(m) + 1
plot, x, W,          $
	color = cgcolor('black'), $
	xtitle = 'i',	$
	psym = 1,	$
	ytitle = 'S!Ii',$
	xrange = [1,200],$
	xstyle = 1,	$
	/ylog

;figure 4.11
sigma = 2.0
m_true = exp( -( (t[0:m-1]-8.0)^2 ) / (2 * sigma^2) ) + $
         0.5 * exp( -( (t[0:m-1]-25.0)^2 ) / (2 * sigma^2) )

plot, t[0:m-1], m_true, $
	color = cgcolor('black'), $
        xtitle = 'Times (s)',   $
        ytitle = 'Accleration (m/s)',$
        xrange = [-5,100],$
        xstyle = 1

;figure 4.12
noise = 0.05
d = G ## m_true
seed = 10.0
dn = d + randomn(seed, m) * noise
plot, t[0:m-1], dn, $
        color = cgcolor('black'), $
        xtitle = 'Times (s)',   $
        ytitle = 'V',$
        xrange = [-5,100],$
        xstyle = 1


; figure 4.13
inv_S = FltArr(m,m)
for i = 0, m-1, 1 do inv_S[i,i] = 1.0 / W[i]
m_inverse = V ## inv_S ## transpose(U) ## d
plot, t[0:m-1], m_inverse, $
        color = cgcolor('black'), $
        xtitle = 'Times (s)',   $
        ytitle = 'Accleration (m/s)',$
        xrange = [-5,100],$
        xstyle = 1

; figure 4.14
mn_inverse = V ## inv_S ## transpose(U) ## dn
plot, t[0:m-1], mn_inverse, $
        color = cgcolor('black'), $
        xtitle = 'Times (s)',   $
        ytitle = 'Accleration (m/s)',$
        xrange = [-5,100],$
        xstyle = 1


; figure 4.15
p = 26
mn_trunc_inverse = V[0:p-1,*] ## inv_S[0:p-1,0:p-1] ## transpose(U[0:p-1,*]) ## dn
plot, t[0:m-1], mn_trunc_inverse, $
        color = cgcolor('black'), $
        xtitle = 'Times (s)',   $
        ytitle = 'Accleration (m/s)',$
        xrange = [-5,100],$
        xstyle = 1

; figure 4.16
Rm_trunc = V[0:p-1,*] ## transpose(V[0:p-1,*])



; figure 4.17
plot, t[0:m-1], Rm_trunc[79,*], $
        color = cgcolor('black'), $
        xtitle = 'Times (s)',   $
        ytitle = 'Element Value',$
        xrange = [-5,100],$
        xstyle = 1



















close_mydevice

stop

end
