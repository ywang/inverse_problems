;
; purpose: to solve example 4.3
; author: Yi Wang
; date: 07/20/2015
;

n = 100

shaw, n, G, theta, s

print, 'G matrix: '
print, G

SVDC, G, W, U, V

;----------------------------
; sort W, U, V desecendingly 
;----------------------------
rank = sort(W)
rank = reverse( rank )
W = W[rank]
for i = 0, n-1, 1 do begin
   tmp_U = U[*,i]
   U[*,i] = tmp_U[rank]
endfor
for i = 0, n-1, 1 do begin
   tmp_V= V[*,i]
   V[*,i] = tmp_V[rank]
endfor

open_mydevice, filename = "example.4.3_2.ps", /portrait

multipanel, rows=3, cols=2

; figure 4.26
x = FindGen(n) + 1
plot, x, W,          $
        color = cgcolor('black'), $
        xtitle = 'i',   $
        psym = 1,       $
        ytitle = 'S!Ii',$
        xrange = [1,100],$
        xstyle = 1,     $
        /ylog


m_50 = dblarr(n)
m_50[49] = 1.0
d_50 = G ## reform(m_50, 1,n)
inv_S = FltArr(n,n)
for i = 0, n-1, 1 do inv_S[i,i] = 1.0 / W[i]
p = 10
noise = 1d-6
seed = 10.0
d_50_noise = d_50 + randomn(seed, n) * noise
m_50_inverse_noise = V[0:p-1,*] ## inv_S[0:p-1,0:p-1] ## transpose(U[0:p-1,*]) ## d_50_noise
plot, theta, m_50_inverse_noise,      $
        color = cgcolor('black'), $
        xtitle = greek('theta')+'(radians)',   $
        psym = -1,       $
        ytitle = 'Intensity',$
        xrange = [-2,2],$
        xstyle = 1




close_mydevice



stop

end
