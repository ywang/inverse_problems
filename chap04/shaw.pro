pro shaw, n, G, theta, s
; 
; purpose: to generate matrix of function 3.11-3.14
; author: Yi Wang
; date: 07/20/2015
;
; notes:
; (1) n should be even
;


if n mod 2 ne 0 then begin
   print, 'n should be even'
   stop
endif

s = dblarr(n)
for i = 0, n-1, 1 do begin
   s[i] = ((i+0.5) * !pi / double(n)) - (!pi / 2d0)
endfor

theta = s

delta_s = !pi / double(n)

G = dblarr(n, n)
for i = 0, n-1, 1 do begin
for j = 0, n-1, 1 do begin

   G[j,i] = delta_s * (cos(s[i])+cos(s[j]))^2 * $
            ( (sin(!pi*(sin(s[i])+sin(s[j])))) / (!pi*(sin(s[i])+sin(s[j]))) )^2

endfor
endfor

end
